using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementUI : MonoBehaviour
{
    public Text achievementNameText;
    public Text achievementDescriptionText;
    public RawImage texture;
    public GameObject achievementPanel;

    public Texture2D i;

    private Queue<Achievement> achievementQueue;
    private AchievementDatabase achievementDatabase;

    private void Awake()
    {
        achievementQueue = new Queue<Achievement>();
        achievementDatabase = GetComponent<AchievementDatabase>();

        AchievementController achievementController = FindObjectOfType<AchievementController>();
        if (achievementController != null)
        {
            achievementController.OnAchievementNotification += HandleAchievementNotification;
        }
    }
    public void AddAchievementToQueue(Achievement achievement)
    {
        achievementQueue.Enqueue(achievement);
        achievementDatabase.UpdateAchievement(achievement);
        if (achievementQueue.Count == 1)
        {
            ShowNextAchievement();
        }
    }

    public void Start()
    {
        achievementPanel.SetActive(false);
    }

    public void ShowNextAchievement()
    {
        if (achievementQueue.Count > 0)
        {
            Achievement achievement = achievementQueue.Dequeue();
            achievementNameText.text = achievement.name;
            achievementDescriptionText.text = achievement.description;
            achievementPanel.SetActive(true);
            MarkAchievementAsUnlocked(achievement.id);
            texture.texture = achievement.image;
            StartCoroutine(HideAchievementPanel(300f));
        }
    }

    private IEnumerator HideAchievementPanel(float delay)
    {
        yield return new WaitForSeconds(delay);

        achievementPanel.SetActive(false);
        if (achievementQueue.Count > 0)
        {
            ShowNextAchievement();
        }
    }
   
    private void HandleAchievementNotification(Achievement achievement)
    {
        ShowAchievementNotification(achievement);
    }
    private void ShowAchievementNotification(Achievement achievement)
    {
        achievementNameText.text = achievement.name;
        achievementDescriptionText.text = achievement.description;
        achievementPanel.SetActive(true);
        texture.texture = achievement.image;
        StartCoroutine(HideAchievementPanel(3f));
    }

    private void MarkAchievementAsUnlocked(int achievementId)
    {
        Achievement[] achievements = achievementDatabase.GetAchievements();
        
        for (int i = 0; i < achievements.Length; i++)
        {
            if (achievements[i].id == achievementId)
            {
                achievements[i].isUnlocked = true;
                achievementDatabase.UpdateAchievement(achievements[i]);
                break;
            }
        }
    }
}