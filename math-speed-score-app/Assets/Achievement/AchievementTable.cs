using Mono.Data.Sqlite;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class AchievementTable : MonoBehaviour
{
    private const string fileName = "db.bytes";
    private const string nameResultTableDB = "achievements";
    private SqliteConnection dbconnection;

    public GameObject table;

    public Texture2D bgUnlock;
    public Texture2D bgLock;

    public int fontSize = 36;

    private List<Achievement> achievements = new List<Achievement>();

    public List<GameObject> existsResult;
    public List<GameObject> noExistsResult;

    void Start()
    {
        setConnection();
        setAchievementsList();
        viewObjectExistsResult(achievements.Count != 0);

        foreach (var item in achievements)
        {
            addRow(item, table);
        }
        closeConnection();
    }

    void Update()
    {
        
    }

    void addRow(Achievement achievement, GameObject table)
    {
        GameObject rowContainerObject = new GameObject("RowContainer");
        rowContainerObject.AddComponent<HorizontalLayoutGroup>();
        rowContainerObject.transform.SetParent(table.transform, false);
        RectTransform parentTransform = table.GetComponent<RectTransform>();
        RectTransform rowContainerTransform = rowContainerObject.GetComponent<RectTransform>();
        rowContainerTransform.sizeDelta = new Vector2(parentTransform.rect.width, fontSize + 160);
        rowContainerObject.GetComponent<HorizontalLayoutGroup>().padding.left = 50;
        rowContainerObject.GetComponent<HorizontalLayoutGroup>().padding.right = 50;
        rowContainerObject.GetComponent<HorizontalLayoutGroup>().padding.top = 10;
        rowContainerObject.GetComponent<HorizontalLayoutGroup>().padding.bottom = 10;

        rowContainerObject.AddComponent<RawImage>();
        if (achievement.isUnlocked)
            rowContainerObject.GetComponent<RawImage>().texture = bgUnlock;
        else
            rowContainerObject.GetComponent<RawImage>().texture = bgLock;


        GameObject iconObjectContainer = new GameObject("IconContainer");
        GameObject iconObject = new GameObject("Icon", typeof(RawImage));
        iconObject.GetComponent<RectTransform>().sizeDelta = new Vector2(120, 120);
        iconObject.transform.SetParent(iconObjectContainer.transform, false);
        RawImage icon = iconObject.GetComponent<RawImage>();
        icon.texture = achievement.image;
        iconObjectContainer.transform.SetParent(rowContainerObject.transform, false);
        iconObjectContainer.AddComponent<LayoutElement>().minWidth = 120;
        iconObjectContainer.GetComponent<LayoutElement>().minHeight = 120;
        iconObjectContainer.GetComponent<LayoutElement>().preferredWidth = 120;
        iconObjectContainer.GetComponent<LayoutElement>().preferredHeight = 120;

        GameObject nameTextObject = new GameObject("Name", typeof(Text));
        Text nameText = nameTextObject.GetComponent<Text>();
        nameText.text = achievement.name;
        nameText.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
        nameText.fontSize = fontSize;
        nameText.alignment = TextAnchor.MiddleLeft;
        nameTextObject.transform.SetParent(rowContainerObject.transform, false);
        nameTextObject.AddComponent<LayoutElement>().flexibleWidth = 4;
        nameTextObject.GetComponent<LayoutElement>().minWidth = 500;
        nameTextObject.GetComponent<LayoutElement>().minHeight = 100;
        nameTextObject.GetComponent<LayoutElement>().preferredWidth = 500;
        nameTextObject.GetComponent<LayoutElement>().preferredHeight = 100;

        GameObject descriptionTextObject = new GameObject("Description", typeof(Text));
        Text descriptionText = descriptionTextObject.GetComponent<Text>();
        descriptionText.text = achievement.description;
        descriptionText.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
        descriptionText.fontSize = fontSize;
        descriptionText.alignment = TextAnchor.MiddleRight;
        descriptionTextObject.transform.SetParent(rowContainerObject.transform, false);
        descriptionTextObject.AddComponent<LayoutElement>().flexibleWidth = 2;
        descriptionTextObject.GetComponent<LayoutElement>().minWidth = 800;
        descriptionTextObject.GetComponent<LayoutElement>().minHeight = 100;
        descriptionTextObject.GetComponent<LayoutElement>().preferredWidth = 800;
        descriptionTextObject.GetComponent<LayoutElement>().preferredHeight = 100;
    }

    void setConnection()
    {
        dbconnection = new SqliteConnection("Data Source=" + GetDatabasePath());
        dbconnection.Open();
    }

    private string GetDatabasePath()
    {
#if UNITY_EDITOR
        return Path.Combine(Application.streamingAssetsPath, fileName);
#elif UNITY_STANDALONE
        string filePath = Path.Combine(Application.dataPath, fileName);
            if(!File.Exists(filePath)) UnpackDatabase(filePath);
            return filePath;
#elif UNITY_ANDROID
            string filePath = Path.Combine(Application.persistentDataPath, fileName);
            if (!File.Exists(filePath)) UnpackDatabase(filePath);
            return filePath;
#endif
    }

    /// <summary> ������������� ���� ������ � ��������� ����. </summary>
    /// <param name="toPath"> ���� � ������� ����� ����������� ���� ������. </param>
    private void UnpackDatabase(string toPath)
    {
        string fromPath = Path.Combine(Application.streamingAssetsPath, fileName);
        WWW reader = new WWW(fromPath);
        while (!reader.isDone) { }

        File.WriteAllBytes(toPath, reader.bytes);
    }

    public void closeConnection()
    {
        dbconnection.Close();
    }

    void setAchievementsList()
    {
        if (dbconnection.State == ConnectionState.Open)
        {
            SqliteCommand cmd = new SqliteCommand();
            cmd.Connection = dbconnection;
            string c = "SELECT * FROM " + nameResultTableDB + " ORDER BY id";
            try
            {
                cmd.CommandText = c;
                SqliteDataReader r = cmd.ExecuteReader();
                try
                {
                    while (r.Read())
                    {
                        int id = r.GetInt32(0);
                        string name = r.GetString(1);
                        string description = r.GetString(2);
                        bool unlocked = r.GetInt32(3) == 1;
                        byte[] imageBytes = GetImageFromReader(r, 4);

                        Texture2D texture = new Texture2D(1, 1);
                        texture.LoadImage(imageBytes);
                        Achievement achievement = new Achievement(id, name, description, unlocked, texture);
                        achievements.Add(achievement);
                    }
                }
                catch { }
            }
            catch (Exception e)
            {
                return;
            }
        }
    }

    private byte[] GetImageFromReader(SqliteDataReader reader, int columnIndex)
    {
        if (!reader.IsDBNull(columnIndex))
        {
            long byteLength = reader.GetBytes(columnIndex, 0, null, 0, 0);
            byte[] buffer = new byte[byteLength];
            reader.GetBytes(columnIndex, 0, buffer, 0, (int)byteLength);
            return buffer;
        }
        else
        {
            return null;
        }
    }

    void viewObjectExistsResult(bool exists)
    {
        if (exists)
        {
            foreach (GameObject item in existsResult)
            {
                item.SetActive(true);
            }
            foreach (GameObject item in noExistsResult)
            {
                item.SetActive(false);
            }
        }
        else
        {
            foreach (GameObject item in existsResult)
            {
                item.SetActive(false);
            }
            foreach (GameObject item in noExistsResult)
            {
                item.SetActive(true);
            }
        }
    }
}
