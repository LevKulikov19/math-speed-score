using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Achievement : MonoBehaviour
{
    public int id;
    public string name;
    public string description;
    public bool isUnlocked;
    public Texture2D image;

    public Achievement(int id, string name, string description, bool isUnlocked, Texture2D image)
    {
        this.id = id;
        this.name = name;
        this.description = description;
        this.isUnlocked = isUnlocked;
        this.image = image;
    }
}