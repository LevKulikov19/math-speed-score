using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GamerResultButtonAction : MonoBehaviour
{
    public void Back()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void PortBack()
    {
        SceneManager.LoadScene("MainMenuPort");
    }
}
