using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using Mono.Data.Sqlite;
using System.Data;
using System.IO;

public class GamerResultTable : MonoBehaviour
{
    private const string fileName = "db.bytes";
    private const string nameResultTableDB = "GamerResult";
    private SqliteConnection dbconnection;

    public GameObject table;
    public int lineWidth = 3;
    public int fontSize = 40;

    private List<Score> gamerResults = new List<Score>();

    public List<GameObject> existsResult;
    public List<GameObject> noExistsResult;

    void Start()
    {
        setConnection();
        setResultsList();
        viewObjectExistsResult(gamerResults.Count != 0);

        foreach (var item in gamerResults)
        {

            addRow(item.NameGamer, difficultyToString(item.difficulty),
                item.dateTime.ToString("MM.dd.yyyy HH:mm"),
                item.score.ToString(), table, 
                item.Equals(gamerResults[gamerResults.Count-1]));
        }

        closeConnection();
    } 

    string difficultyToString (int difficulty)
    {
        switch (difficulty)
        {
            case 1:
                return "Легкий";
            case 2:
                return "Средний";
            case 3:
                return "Трудный";
            default:
                return "Неизвестный";
        }
    }

    void addLine (GameObject table) {
        GameObject lineObject = new GameObject("Line");

        // Устанавливаем родительский объект для линии (Canvas)
        lineObject.transform.SetParent(table.transform, false);

        // Добавляем компонент Line Renderer
        Image lineImage = lineObject.AddComponent<Image>();
        Texture2D texture = new Texture2D(1, lineWidth);
        for (int i = 0; i < lineWidth; i++)
        {
            texture.SetPixel(0, i, Color.white);
        }
        texture.Apply();
        lineImage.sprite = Sprite.Create(texture, new Rect(0, 0, 1, lineWidth), new Vector2(0.5f, 0.5f));

        // Получаем RectTransform линии
        RectTransform lineTransform = lineObject.GetComponent<RectTransform>();

        // Устанавливаем размеры прямоугольника в соответствии с размерами родительского объекта
        RectTransform parentTransform = table.GetComponent<RectTransform>();
        lineTransform.sizeDelta = new Vector2(parentTransform.rect.width, lineWidth);
    }

    void addRow (string name, string difficulty, string date, string result, GameObject table, bool underline = true) {
        GameObject rowContainerObject = new GameObject("RowContainer");
        rowContainerObject.AddComponent<HorizontalLayoutGroup>();
        rowContainerObject.transform.SetParent(table.transform, false);
        RectTransform parentTransform = table.GetComponent<RectTransform>();
        RectTransform rowContainerTransform = rowContainerObject.GetComponent<RectTransform>();
        rowContainerTransform.sizeDelta = new Vector2(parentTransform.rect.width, fontSize+10);

        GameObject nameTextObject = new GameObject("Name", typeof(Text));
        Text nameText = nameTextObject.GetComponent<Text>();
        nameText.text = name;
        nameText.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
        nameText.fontSize = fontSize;
        nameText.alignment = TextAnchor.MiddleLeft;
        nameTextObject.transform.SetParent(rowContainerObject.transform, false);
        nameTextObject.AddComponent<LayoutElement>().minWidth = 100;
        nameTextObject.GetComponent<LayoutElement>().preferredWidth = 150;

        GameObject dateTextObject = new GameObject("Date", typeof(Text));
        Text dateText = dateTextObject.GetComponent<Text>();
        dateText.text = date;
        dateText.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
        dateText.fontSize = fontSize;
        dateText.alignment = TextAnchor.MiddleCenter;
        dateTextObject.transform.SetParent(rowContainerObject.transform, false);
        dateTextObject.AddComponent<LayoutElement>().minWidth = 100;
        dateTextObject.GetComponent<LayoutElement>().preferredWidth = 150;

        GameObject difficyltyTextObject = new GameObject("Difficulty", typeof(Text));
        Text difficyltyText = difficyltyTextObject.GetComponent<Text>();
        difficyltyText.text = difficulty;
        difficyltyText.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
        difficyltyText.fontSize = fontSize;
        difficyltyText.alignment = TextAnchor.MiddleCenter;
        difficyltyTextObject.transform.SetParent(rowContainerObject.transform, false);
        difficyltyTextObject.AddComponent<LayoutElement>().minWidth = 100;
        difficyltyTextObject.GetComponent<LayoutElement>().preferredWidth = 150;

        GameObject resultTextObject = new GameObject("Result", typeof(Text));
        Text resultText = resultTextObject.GetComponent<Text>();
        resultText.text = result;
        resultText.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
        resultText.fontSize = fontSize;
        resultText.alignment = TextAnchor.MiddleRight;
        resultTextObject.transform.SetParent(rowContainerObject.transform, false);
        resultTextObject.AddComponent<LayoutElement>().minWidth = 100;
        resultTextObject.GetComponent<LayoutElement>().preferredWidth = 150;

        if (underline) addLine(table);
    }

    void setConnection ()
    {
        dbconnection = new SqliteConnection("Data Source=" + GetDatabasePath());
        dbconnection.Open();
    }

    private string GetDatabasePath()
    {
        #if UNITY_EDITOR
                return Path.Combine(Application.streamingAssetsPath, fileName);
        #elif UNITY_STANDALONE
        string filePath = Path.Combine(Application.dataPath, fileName);
            if(!File.Exists(filePath)) UnpackDatabase(filePath);
            return filePath;
        #elif UNITY_ANDROID
            string filePath = Path.Combine(Application.persistentDataPath, fileName);
            if (!File.Exists(filePath)) UnpackDatabase(filePath);
            return filePath;
        #endif
    }

    /// <summary> Распаковывает базу данных в указанный путь. </summary>
    /// <param name="toPath"> Путь в который нужно распаковать базу данных. </param>
    private void UnpackDatabase(string toPath)
    {
        string fromPath = Path.Combine(Application.streamingAssetsPath, fileName);
        WWW reader = new WWW(fromPath);
        while (!reader.isDone) { }

        File.WriteAllBytes(toPath, reader.bytes);
    }

    public void closeConnection()
    {
        dbconnection.Close();
    }

    void setResultsList()
    {
        if (dbconnection.State == ConnectionState.Open)
        {
            try
            {
                createResultTable();
            }
            catch (Exception e)
            {

            }
            SqliteCommand cmd = new SqliteCommand();
            cmd.Connection = dbconnection;
            string c = "SELECT * FROM " + nameResultTableDB + " ORDER BY score, difficulty DESC LIMIT 50";
            try
            {
                cmd.CommandText = c;
                SqliteDataReader r = cmd.ExecuteReader();
                try
                {
                    while (r.Read())
                    {
                        gamerResults.Add(new Score(
                            r.GetString(0),
                            r.GetInt32(1),
                            DateTime.Parse((string)r.GetString(2)),
                            (uint)r.GetInt32(3)));
                    }
                }
                catch { }
            }
            catch (Exception e)
            {
                viewObjectExistsResult(false);
                return;
            }

            viewObjectExistsResult(true);
        }
    }

    bool isExistsResultTable ()
    {
        try
        {
            SqliteCommand cmd = new SqliteCommand();
            cmd.Connection = dbconnection;
            cmd.CommandText = "SHOW TABLES FROM 'db_name' LIKE " + nameResultTableDB;
            SqliteDataReader r = cmd.ExecuteReader();
            while (r.Read())
                Debug.Log(r[0]);
        }
        catch (Exception e)
        {
            return false;
        }

        return true;
    }

    void createResultTable()
    {
        SqliteCommand cmd = new SqliteCommand();
        cmd.Connection = dbconnection;
        cmd.CommandText = "CREATE TABLE " + nameResultTableDB  + " (name VARCHAR(255), difficulty INT, date VARCHAR(255), score INT);";
        cmd.ExecuteReader();
    }

    void viewObjectExistsResult (bool exists)
    {
        if (exists)
        {
            foreach (GameObject item in existsResult)
            {
                item.SetActive(true);
            }
            foreach (GameObject item in noExistsResult)
            {
                item.SetActive(false);
            }
        }
        else
        {
            foreach (GameObject item in existsResult)
            {
                item.SetActive(false);
            }
            foreach (GameObject item in noExistsResult)
            {
                item.SetActive(true);
            }
        }
    }


}
