using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckButton : MonoBehaviour
{
    public Text inputText;

    public ScoreManager scoreManager;
    public LivesManager livesManager;

    private Button button;

    float startTime;

    public AchievementController achcontrol;
    private void Update()
    {
        if( Input.GetKeyDown( KeyCode.KeypadEnter) ) 
        {
            OnButtonClick();
        }

        startTime = Time.time;
    }

    public void OnButtonClick()
    {
        if(CheckGoldenCloudsOnAnswer(FindObjectsOfType(typeof(GoldenCloud))))
        {
            DestroyAllClouds();
        }      
        if(CheckSimpleCloudsOnAnswer(FindObjectsOfType(typeof(SimpleCloud)))) 
        {
            DestroySimpleClouds();
        }   
        if(CheckHealthCloudsOnAnswer(FindObjectsOfType(typeof(HealthCloud))))
        {
            DestroyHealthClouds();
        }    
        inputText.text = "";

    }
    private bool CheckHealthCloudsOnAnswer(Object[] healthClouds)
    {
        for (int i = 0; i < healthClouds.Length; i++)
        {
            HealthCloud hCl = (HealthCloud)healthClouds[i];
            if(hCl != null && inputText.text.Equals(hCl.ReResult().ToString()))
            {
                return true;
            }
        }
        return false;
    }
    private bool CheckGoldenCloudsOnAnswer(Object[] goldenClouds)
    {
        for (int i = 0; i < goldenClouds.Length; i++)
        {
            GoldenCloud gCl = (GoldenCloud)goldenClouds[i];
            if(gCl != null && inputText.text.Equals(gCl.ReResult().ToString()))
            {
                return true;
            }
        }
        return false;
    }
    private bool CheckSimpleCloudsOnAnswer(Object[] simpleClouds)
    {
        for (int i = 0; i < simpleClouds.Length;i++)
        {
            SimpleCloud sCl = (SimpleCloud)simpleClouds[i];
            if(sCl != null && inputText.text.Equals(sCl.ReResult().ToString()))
            {
                return true;
            }
        }
        return false;
    }
    private void DestroyAllClouds()
    {       
        foreach(SimpleCloud sCl in FindObjectsOfType(typeof(SimpleCloud)))
        {
            Destroy(sCl.gameObject);
            scoreManager.IncreaseScore();
        }
        foreach(GoldenCloud bCl in FindObjectsOfType(typeof(GoldenCloud)))
        {
            Destroy(bCl.gameObject);
            scoreManager.IncreaseScore();
        }
        foreach(HealthCloud hCl in FindObjectsOfType(typeof(HealthCloud)))
        {
            Destroy(hCl.gameObject);
            scoreManager.IncreaseScore();
            LivesManager.Instance.PushLives();
        }

        AchievementController.checkneed = true;
    }
    private void DestroyHealthClouds()
    {
        foreach(HealthCloud hCl in FindObjectsOfType(typeof(HealthCloud)))
        {
            if(hCl != null && inputText.text.Equals(hCl.ReResult().ToString()))
            {
                Destroy(hCl.gameObject);
                scoreManager.IncreaseScore();
                LivesManager.Instance.PushLives();            
            }
        }
        AchievementController.checkneed = true;
    }
    private void DestroySimpleClouds()
    {
        foreach(SimpleCloud sCl in FindObjectsOfType(typeof(SimpleCloud)))
        {
            if(sCl != null && inputText.text.Equals(sCl.ReResult().ToString()))
            {
                Destroy(sCl.gameObject);
                scoreManager.IncreaseScore();
            }
        }
        AchievementController.checkneed = true;
    }
}