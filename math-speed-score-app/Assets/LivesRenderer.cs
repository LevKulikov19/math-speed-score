using UnityEngine;
using UnityEngine.UI;

public class LivesRenderer : MonoBehaviour
{
    public Image lifeImagePrefab;
    public Transform livesContainer;
    public Vector2 lifeImageSpacing;
    public int maxLives = 3;

    private Image[] lifeImages;

    private void Start()
    {
        CreateLifeImages();
    }

    private void CreateLifeImages()
    {
        lifeImages = new Image[maxLives];

        for (int i = 0; i < maxLives; i++)
        {
            Image lifeImage = Instantiate(lifeImagePrefab, livesContainer);
            lifeImage.rectTransform.localPosition = new Vector2(i * lifeImageSpacing.x, i * lifeImageSpacing.y);
            lifeImages[i] = lifeImage;
        }
    }

    public void UpdateLives(int currentLives)
    {
        for (int i = 0; i < maxLives; i++)
        {
            if (i < currentLives)
            {
                lifeImages[i].enabled = true;
            }
            else
            {
                lifeImages[i].enabled = false;
            }
        }
    }
}