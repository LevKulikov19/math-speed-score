using Mono.Data.Sqlite;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LivesManager : MonoBehaviour
{
    public static LivesManager Instance { get; private set; }
    public int startingLives = 3;
    private int currentLives;

    public LivesRenderer livesRenderer;

    private const string fileName = "db.bytes";
    private const string nameResultTableDB = "GamerResult";
    private SqliteConnection dbconnection;
    public int orientetion;

    public GameObject GameScore;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        currentLives = startingLives;
        UpdateLivesRenderer();
    }

    public void DecreaseLives()
    {
        currentLives--;

        if (currentLives <= 0)
        {
            GameOver();
        }
        UpdateLivesRenderer();
    }
    public void PushLives()
    {
        if (currentLives < 3)
        {
            currentLives++;
        }
        UpdateLivesRenderer();
    }
    private void UpdateLivesRenderer()
    {
        livesRenderer.UpdateLives(currentLives);
    }
    private void GameOver()
    {      

        UnityEngine.Debug.Log(orientetion);
        setConnection();
        int a = GameScore.GetComponent<ScoreManager>().ReResult();
        addScore(a);
        if (orientetion == 1)
        {
            SceneManager.LoadScene("GameOver");
        }
        if (orientetion == 2)
        {
            SceneManager.LoadScene("PortGameOver");
        }
        
    }

    void setConnection()
    {
        dbconnection = new SqliteConnection("Data Source=" + GetDatabasePath());
        dbconnection.Open();
    }

    private string GetDatabasePath()
    {
        #if UNITY_EDITOR
            return Path.Combine(Application.streamingAssetsPath, fileName);
        #elif UNITY_STANDALONE
            string filePath = Path.Combine(Application.dataPath, fileName);
            if(!File.Exists(filePath)) UnpackDatabase(filePath);
            return filePath;
        #elif UNITY_ANDROID
            string filePath = Path.Combine(Application.persistentDataPath, fileName);
 //           if (!File.Exists(filePath)) UnpackDatabase(filePath);
            return filePath;
        #endif
    }

    /// <summary> ������������� ���� ������ � ��������� ����. </summary>
    /// <param name="toPath"> ���� � ������� ����� ����������� ���� ������. </param>
    private void UnpackDatabase(string toPath)
    {
        string fromPath = Path.Combine(Application.streamingAssetsPath, fileName);
        Debug.Log(fromPath);
        WWW reader = new WWW(fromPath);
        while (!reader.isDone) { }

        File.WriteAllBytes(toPath, reader.bytes);
    }

    private void addScore(int score)
    {
        if (dbconnection.State == ConnectionState.Open)
        {
            try
            {
                createResultTable();
            }
            catch (Exception e) { }

            if (score > 0)
            {
                SqliteCommand cmd = new SqliteCommand();
                cmd.Connection = dbconnection;
                string c = "INSERT INTO " + nameResultTableDB +
                    " VALUES('" + UserTable.selectUser.NameGamer + "', '" + RandomSpawn.difficulty.ToString() + "', '" + 
                    DateTime.Now.ToLocalTime() + "', '" + score.ToString() + "');";
                Debug.Log(c);
                cmd.CommandText = c;
                cmd.ExecuteReader();
            }
        }
    }

    void createResultTable()
    {
        SqliteCommand cmd = new SqliteCommand();
        cmd.Connection = dbconnection;
        cmd.CommandText = "CREATE TABLE " + nameResultTableDB + " (name VARCHAR(255), difficulty INT, date VARCHAR(255), score INT);";
        cmd.ExecuteReader();
    }
}