using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;


public class ScoreManager : MonoBehaviour
{
    public static ScoreManager Instance { get; private set; }

    [SerializeField]
    private int scorePerLife = 10;
    public Text outputtext;

    private int score;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        score = 0;
    }

    public void UpdateScore()
    {
        score = 0;
    }

    public int ReResult()
    {
        return score;
    }

    public void IncreaseScore()
    {
        score++;
        outputtext.text = "Score: " + score.ToString();

        if (score % scorePerLife == 0)
        {
            IncreaseLives();
        }
    }

    private void IncreaseLives()
    {
        LivesManager.Instance.PushLives();
    }


}