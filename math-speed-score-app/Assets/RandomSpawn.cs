using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawn : MonoBehaviour
{
    [SerializeField]
    private GameObject cloudPrefab;
    [SerializeField]
    private GameObject bonusCloudPrefab;
    [SerializeField]
    private GameObject healthCloudPrefab;
    [SerializeField]
    private GameObject numberPrefab;
    [SerializeField]
    private Transform numberParent;
    [SerializeField]
    private float spawnRate;
    public static bool acceleration = true;
    private float nextAccelerationTime = 0.0f;
    public static int difficulty = 3;
    [SerializeField]
    public int orientationSpawn;

    private float simpleCloudSpeed = 30.0f;
    private float bonusCloudSpeed = 50.0f;

    Vector2 whereToSpawn;
    float RandX = 0.0f;
    private float nextSpawn = 0.0f;
    
    private bool IsDiffucultyIncreaseNeeded = false;
    string cloudType;

    private float overlapRadius = 2f;
    
    private void Start()
    {
        spawnRate = 5.0f;
        cloudType = "";
    }
    private void Update()
    {
        if (acceleration && Time.time > nextAccelerationTime)
        {
            IncreaseCloudsSpeed();
        }
        if (Time.time > nextSpawn)
        {
            nextSpawn = Time.time + spawnRate;
            SpawnCloud();
        }
    }
    private void IncreaseCloudsSpeed()
    {
        switch (difficulty)
        {
            case 1:
            {
                simpleCloudSpeed += 2.5f;
                bonusCloudSpeed += 0.5f;
                nextAccelerationTime = Time.time + 30;
                spawnRate -= 0.1f;
                break;
            }
            case 2:
            {
                simpleCloudSpeed += 1.5f;
                bonusCloudSpeed += 0.25f;
                nextAccelerationTime = Time.time + 30;
                spawnRate -= 0.075f;
                break;
            }
            case 3: 
            {
                simpleCloudSpeed += 0.5f;
                bonusCloudSpeed += 0.1f;
                nextAccelerationTime = Time.time + 30;
                spawnRate -= 0.05f;
                break;
            }
            default: break;
        }
    }

    private GameObject GetCloudTypeForSpawn()
    {
        GameObject cloud = null;

        System.Random random = new System.Random();
        int number = random.Next(101);
        if(number > 5)
        {
            //simple cloud
            cloudType = "Simple";
            IsDiffucultyIncreaseNeeded = false;
            cloud = Instantiate(cloudPrefab,whereToSpawn,Quaternion.identity);
            return cloud;
        }
        else
        {
            int randomChoose = random.Next(1,3);
            IsDiffucultyIncreaseNeeded = true; //Увеличим сложность уравнения для редких бонусных облаков.
            switch (randomChoose)
            {
                case 1: 
                {                
                    //Golden cloud
                    cloudType = "Golden";
                    cloud = Instantiate(bonusCloudPrefab, whereToSpawn, Quaternion.identity);
                    return cloud;
                    break;
                }
                case 2:
                {        
                    //Health cloud
                    cloudType = "Health";
                    cloud = Instantiate(healthCloudPrefab, whereToSpawn, Quaternion.identity);       
                    return cloud;
                    break;
                }
                default: break;
            }
            
        }
        return cloud;
    }

    private void SetCloudData(GameObject _cloud, string _expression, int _result)
    {
        switch(cloudType)
        {
            case "Simple":
            {
                _cloud.GetComponent<SimpleCloud>().SetProblem(_expression, _result); 
                _cloud.GetComponent<SimpleCloud>().SetSpeed(simpleCloudSpeed);
                break;              
            }
            case "Golden":
            {
                _cloud.GetComponent<GoldenCloud>().SetProblem(_expression, _result);  
                _cloud.GetComponent<GoldenCloud>().SetSpeed(bonusCloudSpeed);
                break;    
            }
            case "Health":
            {
                _cloud.GetComponent<HealthCloud>().SetProblem(_expression, _result);   
                _cloud.GetComponent<HealthCloud>().SetSpeed(bonusCloudSpeed);
                break;    
            }
            default: break;
        }
        _cloud.GetComponent<TextMesh>().text = _expression;
    }
    private void SpawnCloud()
    {
        if (orientationSpawn == 1)
        {
            RandX = UnityEngine.Random.Range(-600f, 600);
        }
        if (orientationSpawn == 2)
        {
            RandX = UnityEngine.Random.Range(-200f, 200);
        }      
        whereToSpawn = new Vector2(RandX, transform.position.y);
        Collider2D[] colliders = Physics2D.OverlapCircleAll(whereToSpawn, overlapRadius);
        if (colliders.Length > 0)
        {
            return;
        }
        GameObject cloud = GetCloudTypeForSpawn();
        ExpressionLogicClass logic = new ExpressionLogicClass(IsDiffucultyIncreaseNeeded ? difficulty + 1 : difficulty);
        logic.CreateExpression();
        string expression = logic.ReturnStringExpression();
        int result = logic.ReturnExpressionResult();

        SetCloudData(cloud, expression, result);
    }
}