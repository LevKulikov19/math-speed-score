using Mono.Data.Sqlite;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class UserTable : MonoBehaviour
{
    private const string fileName = "db.bytes";
    private const string nameUserTableDB = "User";
    private const string nameResultTableDB = "GamerResult";
    private SqliteConnection dbconnection;

    public Text textActiveUser;
    public GameObject table;
    public int fontSize = 36;
    private int lineWidth = 3;

    private List<User> users = new List<User>();

    public Texture2D deleteUserIcon;
    public Texture2D deleteUserDisableIcon;
    public Texture2D selectUserIcon;
    public Texture2D selectUserDisableIcon;

    public static User selectUser = null;

    void Start()
    {
        refreshTable();
    }

    public static void RemoveChildren(Transform transform)
    {
        for (int i = transform.childCount - 1; i >= 0; i--)
        {
            GameObject.Destroy(transform.GetChild(i).gameObject);
        }
    }

    void refreshTable()
    {
        loadBestResultUser();
        users = new List<User>();
        setConnection();
        setResultsList();
        RemoveChildren(table.transform);
        foreach (var item in users)
        {
            addRow(item, table,
                !item.Equals(users[users.Count - 1]));
        }
        closeConnection();
        loadSelectUser();
        setConnection();
        setActiveUserLable();
        closeConnection();
    }

    void addLine(GameObject table)
    {
        GameObject lineObject = new GameObject("Line");

        // ������������� ������������ ������ ��� ����� (Canvas)
        lineObject.transform.SetParent(table.transform, false);

        // ��������� ��������� Line Renderer
        Image lineImage = lineObject.AddComponent<Image>();
        Texture2D texture = new Texture2D(1, lineWidth);
        for (int i = 0; i < lineWidth; i++)
        {
            texture.SetPixel(0, i, Color.white);
        }
        texture.Apply();
        lineImage.sprite = Sprite.Create(texture, new Rect(0, 0, 1, lineWidth), new Vector2(0.5f, 0.5f));

        // �������� RectTransform �����
        RectTransform lineTransform = lineObject.GetComponent<RectTransform>();

        // ������������� ������� �������������� � ������������ � ��������� ������������� �������
        RectTransform parentTransform = table.GetComponent<RectTransform>();
        lineTransform.sizeDelta = new Vector2(parentTransform.rect.width, lineWidth);
    }

    void addRow(User user, GameObject table, bool underline = true)
    {
        GameObject rowContainerObject = new GameObject("RowContainer");
        rowContainerObject.AddComponent<HorizontalLayoutGroup>();
        rowContainerObject.transform.SetParent(table.transform, false);
        RectTransform parentTransform = table.GetComponent<RectTransform>();
        RectTransform rowContainerTransform = rowContainerObject.GetComponent<RectTransform>();
        rowContainerTransform.sizeDelta = new Vector2(parentTransform.rect.width, fontSize + 50);
        rowContainerObject.GetComponent<HorizontalLayoutGroup>().padding.left = 50;
        rowContainerObject.GetComponent<HorizontalLayoutGroup>().padding.right = 0;
        rowContainerObject.GetComponent<HorizontalLayoutGroup>().padding.top = 10;
        rowContainerObject.GetComponent<HorizontalLayoutGroup>().padding.bottom = 10;

        GameObject nameTextObject = new GameObject("Name", typeof(Text));
        Text nameText = nameTextObject.GetComponent<Text>();
        nameText.text = user.NameGamer;
        nameText.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
        nameText.fontSize = fontSize;
        nameText.alignment = TextAnchor.MiddleLeft;
        nameTextObject.transform.SetParent(rowContainerObject.transform, false);
        nameTextObject.AddComponent<LayoutElement>().minWidth = 200;
        nameTextObject.GetComponent<LayoutElement>().preferredWidth = 200;
        nameTextObject.GetComponent<LayoutElement>().flexibleWidth = 4;

        GameObject resultTextObject = new GameObject("Result", typeof(Text));
        Text resultText = resultTextObject.GetComponent<Text>();
        resultText.text = user.bestResult.ToString();
        resultText.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
        resultText.fontSize = fontSize;
        resultText.alignment = TextAnchor.MiddleCenter;
        resultTextObject.transform.SetParent(rowContainerObject.transform, false);
        resultTextObject.AddComponent<LayoutElement>().minWidth = 200;
        resultTextObject.GetComponent<LayoutElement>().preferredWidth = 200;
        resultTextObject.GetComponent<LayoutElement>().flexibleWidth = 4;

        GameObject iconObjectContainer = new GameObject("IconContainer");
        GameObject iconObject = new GameObject("Icon", typeof(RawImage));
        iconObject.GetComponent<RectTransform>().sizeDelta = new Vector2(60, 60);
        iconObject.transform.SetParent(iconObjectContainer.transform, false);
        RawImage icon = iconObject.GetComponent<RawImage>();
        if (user.isSelect)
            icon.texture = selectUserDisableIcon;
        else
        {
            icon.texture = selectUserIcon;
            iconObject.AddComponent<Button>().onClick.AddListener(() => SelectUserOnClick(user));
        }
        iconObjectContainer.transform.SetParent(rowContainerObject.transform, false);
        iconObjectContainer.AddComponent<LayoutElement>().minWidth = 60;
        iconObjectContainer.GetComponent<LayoutElement>().minHeight = 60;
        iconObjectContainer.GetComponent<LayoutElement>().preferredWidth = 60;
        iconObjectContainer.GetComponent<LayoutElement>().preferredHeight = 60;

        GameObject iconSelectObjectContainer = new GameObject("IconContainer");
        GameObject iconSelectObject = new GameObject("Icon", typeof(RawImage));
        iconSelectObject.GetComponent<RectTransform>().sizeDelta = new Vector2(60, 60);
        iconSelectObject.transform.SetParent(iconSelectObjectContainer.transform, false);
        RawImage iconSelect = iconSelectObject.GetComponent<RawImage>();
        if (user.isSelect)
            iconSelect.texture = deleteUserDisableIcon;
        else
        {
            iconSelect.texture = deleteUserIcon;
            iconSelectObject.AddComponent<Button>().onClick.AddListener(() => DeleteUserOnClick(user));
        }
        iconSelectObjectContainer.transform.SetParent(rowContainerObject.transform, false);
        iconSelectObjectContainer.AddComponent<LayoutElement>().minWidth = 60;
        iconSelectObjectContainer.GetComponent<LayoutElement>().minHeight = 60;
        iconSelectObjectContainer.GetComponent<LayoutElement>().preferredWidth = 60;
        iconSelectObjectContainer.GetComponent<LayoutElement>().preferredHeight = 60;

        if (underline) addLine(table);
    }

    void DeleteUserOnClick(User user)
    {
        DeleteUser(user);
        setConnection();
        refreshTable();
        closeConnection();
    }

    void SelectUserOnClick(User user)
    {
        updateSelectUser(user.NameGamer);
        setConnection();
        refreshTable();
        closeConnection();
    }

    void setConnection()
    {
        dbconnection = new SqliteConnection("Data Source=" + GetDatabasePath());
        dbconnection.Open();
    }

    private string GetDatabasePath()
    {
#if UNITY_EDITOR
        return Path.Combine(Application.streamingAssetsPath, fileName);
#elif UNITY_STANDALONE
        string filePath = Path.Combine(Application.dataPath, fileName);
            if(!File.Exists(filePath)) UnpackDatabase(filePath);
            return filePath;
#elif UNITY_ANDROID
            string filePath = Path.Combine(Application.persistentDataPath, fileName);
            if (!File.Exists(filePath)) UnpackDatabase(filePath);
            return filePath;
#endif
    }

    /// <summary> ������������� ���� ������ � ��������� ����. </summary>
    /// <param name="toPath"> ���� � ������� ����� ����������� ���� ������. </param>
    private void UnpackDatabase(string toPath)
    {
        string fromPath = Path.Combine(Application.streamingAssetsPath, fileName);
        WWW reader = new WWW(fromPath);
        while (!reader.isDone) { }

        File.WriteAllBytes(toPath, reader.bytes);
    }

    public void closeConnection()
    {
        dbconnection.Close();
    }

    void createUserTable()
    {
        SqliteCommand cmd = new SqliteCommand();
        cmd.Connection = dbconnection;
        cmd.CommandText = "CREATE TABLE " + nameUserTableDB + " (name VARCHAR(255) NOT NULL PRIMARY KEY, score INT, select_user INT);";
        cmd.ExecuteReader();
    }

    void createResultTable()
    {
        SqliteCommand cmd = new SqliteCommand();
        cmd.Connection = dbconnection;
        cmd.CommandText = "CREATE TABLE " + nameResultTableDB + " (name VARCHAR(255), difficulty INT, date VARCHAR(255), score INT);";
        cmd.ExecuteReader();
    }

    void setResultsList()
    {
        if (dbconnection.State == ConnectionState.Open)
        {
            try
            {
                createUserTable();
            }
            catch (Exception e)
            {
                Debug.LogWarning(e.Message);
            }
            SqliteCommand cmd = new SqliteCommand();
            cmd.Connection = dbconnection;
            string c = "SELECT * FROM " + nameUserTableDB;
            try
            {
                cmd.CommandText = c;
                SqliteDataReader r = cmd.ExecuteReader();
                try
                {
                    while (r.Read())
                    {
                        users.Add(new User(
                            r.GetString(0),
                            (uint)r.GetInt32(1),
                            r.GetBoolean(2)));
                    }
                }
                catch { }
            }
            catch { }
        }
    }

    void loadBestResultUser ()
    {
        setConnection();
        if (dbconnection.State == ConnectionState.Open)
        {
            try
            {
                createResultTable();
            }
            catch (Exception e)
            {
                Debug.LogWarning(e.Message);
            }

            try
            {
                createUserTable();
            }
            catch (Exception e)
            {
                Debug.LogWarning(e.Message);
            }

            List<string> username = new List<string>();
            try
            {
                SqliteCommand cmd = new SqliteCommand();
                cmd.Connection = dbconnection;
                string c = "SELECT * FROM " + nameUserTableDB;
                cmd.CommandText = c;
                SqliteDataReader r = cmd.ExecuteReader();
                try
                {
                    while (r.Read())
                        username.Add(r.GetString(0));
                }
                catch { }
            }
            catch { }



            try
            {
                foreach (string item in username)
                {
                    SqliteCommand cmd = new SqliteCommand();
                    cmd.Connection = dbconnection;
                    string c = "SELECT * FROM " + nameResultTableDB + " WHERE name = '" + item + "' ORDER BY score DESC LIMIT 1";
                    cmd.CommandText = c;
                    SqliteDataReader r = cmd.ExecuteReader();
                    try
                    {
                        r.Read();
                        string c1 = "UPDATE " + nameUserTableDB + " SET score = " + r.GetInt32(3).ToString() + " WHERE name = '" + item + "'";
                        Debug.Log(c1);
                        cmd.Dispose();
                        cmd = new SqliteCommand();
                        cmd.Connection = dbconnection;
                        cmd.CommandText = c1;
                        cmd.ExecuteReader();
                    }
                    catch { }
                }
            }
            catch (Exception e) { }
        }
        closeConnection();
    }

    public void addUser(string userName)
    {
        setConnection();
        if (dbconnection.State == ConnectionState.Open)
        {
            try
            {
                createUserTable();
            }
            catch (Exception e) { }

            SqliteCommand cmd = new SqliteCommand();
            cmd.Connection = dbconnection;
            string c = "INSERT INTO " + nameUserTableDB +
                " VALUES('" + userName + "', '" + 0 + "', '" + 0 + "');";
            Debug.Log(c);
            cmd.CommandText = c;
            cmd.ExecuteReader();
        }
        closeConnection();
        updateSelectUser(userName);
        refreshTable();
    }

    public bool isExistsUserName(string name)
    {
        setConnection();
        if (dbconnection.State == ConnectionState.Open)
        {
            try
            {
                createUserTable();
            }
            catch (Exception e) { }

            SqliteCommand cmd = new SqliteCommand();
            cmd.Connection = dbconnection;
            string c = "SELECT * FROM " + nameUserTableDB;
            try
            {
                cmd.CommandText = c;
                SqliteDataReader r = cmd.ExecuteReader();
                try
                {
                    while (r.Read())
                    {
                        if (r.GetString(0) == name) return true;
                    }
                }
                catch { }
            }
            catch (Exception e) { }

        }
        closeConnection();
        return false;
    }

    public void updateSelectUser(string name)
    {
        setConnection();
        if (dbconnection.State == ConnectionState.Open)
        {
            try
            {
                createUserTable();
            }
            catch (Exception e) { }

            List<string> username = new List<string>();

            string c = "SELECT * FROM " + nameUserTableDB + " WHERE select_user = 1";
            try
            {
                SqliteCommand cmd = new SqliteCommand();
                cmd.Connection = dbconnection;
                cmd.CommandText = c;
                SqliteDataReader r = cmd.ExecuteReader();
                try
                {
                    while (r.Read())
                    {
                        username.Add(r.GetString(0));
                    }
                }
                catch { }
            }
            catch (Exception e) { }

            foreach (string item in username)
            {
                SqliteCommand cmd = new SqliteCommand();
                cmd.Connection = dbconnection;
                c = "UPDATE " + nameUserTableDB + " SET select_user = 0 WHERE name = '" + item +"'";
                try
                {
                    cmd.CommandText = c;
                    SqliteDataReader r = cmd.ExecuteReader();
                }
                catch (Exception e) { }
            }

            try
            {
                c = "UPDATE " + nameUserTableDB + " SET select_user = 1 WHERE name = '" + name + "'";
                Debug.Log(c);
                SqliteCommand cmd = new SqliteCommand();
                cmd.Connection = dbconnection;
                cmd.CommandText = c;
                SqliteDataReader r = cmd.ExecuteReader();
            }
            catch (Exception e) { }

        }
        closeConnection();
        loadSelectUser();
    }

    public void loadSelectUser()
    {
        setConnection();
        if (dbconnection.State == ConnectionState.Open)
        {
            try
            {
                createUserTable();
            }
            catch (Exception e) { }


            SqliteCommand cmd = new SqliteCommand();
            cmd.Connection = dbconnection;
            string c = "SELECT * FROM " + nameUserTableDB + " WHERE select_user = 1";
            try
            {
                cmd.CommandText = c;
                SqliteDataReader r = cmd.ExecuteReader();
                try
                {
                    r.Read();
                    selectUser = new User(r.GetString(0), (uint)r.GetInt32(1), r.GetBoolean(2));
                }
                catch { }
            }
            catch (Exception e) { }
        }
        closeConnection();
    }

    void setActiveUserLable()
    {
        if (selectUser == null)
            textActiveUser.text = "�������� ������������ \n�� ������";
        else
            textActiveUser.text = "�������� ������������ \n" + selectUser.NameGamer;
    }

    void DeleteUser(User user)
    {
        setConnection();
        if (dbconnection.State == ConnectionState.Open)
        {
            try
            {
                createUserTable();
            }
            catch (Exception e) { }

            try
            {
                string c = "DELETE FROM " + nameUserTableDB + " WHERE name = '" + user.NameGamer + "'";
                Debug.Log(c);
                SqliteCommand cmd = new SqliteCommand();
                cmd.Connection = dbconnection;
                cmd.CommandText = c;
                SqliteDataReader r = cmd.ExecuteReader();
            }
            catch (Exception e) { }
        }

        closeConnection();
    }
}
