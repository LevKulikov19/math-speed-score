using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class User
{
    private string nameGamer;
    public string NameGamer
    {
        get => nameGamer;
        set
        {
            if (value.Length > 30)
                nameGamer = value.Substring(0, 30) + "...";
            else
                nameGamer = value;
        }
    }
    public uint bestResult;
    public bool isSelect;

    public User(string name, uint bestResult, bool select)
    {
        nameGamer = name;
        this.bestResult = bestResult;
        this.isSelect = select;
    }
}
