using Mono.Data.Sqlite;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SampleSceneAction : MonoBehaviour
{
    private const string fileName = "db.bytes";
    private const string nameResultTableDB = "GamerResult";
    private SqliteConnection dbconnection;
    public int orientetion;

    public GameObject GameScore;

    public void Back()
    {
        setConnection();
        int a = GameScore.GetComponent<ScoreManager>().ReResult();
        addScore(a);
        if (orientetion == 1)
        {
            SceneManager.LoadScene("MainMenu");
        }

        if (orientetion == 2)
        {
            SceneManager.LoadScene("MainMenuPort");
        }
    }

    void setConnection()
    {
        dbconnection = new SqliteConnection("Data Source=" + GetDatabasePath());
        dbconnection.Open();
    }

    private string GetDatabasePath()
    {
#if UNITY_EDITOR
        return Path.Combine(Application.streamingAssetsPath, fileName);
#elif UNITY_STANDALONE
            string filePath = Path.Combine(Application.dataPath, fileName);
            if(!File.Exists(filePath)) UnpackDatabase(filePath);
            return filePath;
#elif UNITY_ANDROID
            string filePath = Path.Combine(Application.persistentDataPath, fileName);
//            if (!File.Exists(filePath)) UnpackDatabase(filePath);
            return filePath;
#endif
    }

    private void addScore(int score)
    {
        if (dbconnection.State == ConnectionState.Open)
        {
            try
            {
                createResultTable();
            }
            catch (Exception e) { }

            if (score > 0)
            {
                SqliteCommand cmd = new SqliteCommand();
                cmd.Connection = dbconnection;
                string c = "INSERT INTO " + nameResultTableDB +
                    " VALUES('" + UserTable.selectUser.NameGamer + "', '" + RandomSpawn.difficulty.ToString() + "', '" +
                    DateTime.Now.ToLocalTime() + "', '" + score.ToString() + "');";
                Debug.Log(c);
                cmd.CommandText = c;
                cmd.ExecuteReader();
            }
        }
    }

    void createResultTable()
    {
        SqliteCommand cmd = new SqliteCommand();
        cmd.Connection = dbconnection;
        cmd.CommandText = "CREATE TABLE " + nameResultTableDB + " (name VARCHAR(255), difficulty INT, date VARCHAR(255), score INT);";
        cmd.ExecuteReader();
    }
}
