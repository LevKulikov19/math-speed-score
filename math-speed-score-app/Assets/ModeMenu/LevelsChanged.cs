using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class LevelsChanged : MonoBehaviour
{
    public Text difficulty_text;
    public Toggle toggle;
    Button OnlySummationButton;
    Button OnlySubstractionButton;
    Button OnlyMultiplicationButton;
    Button OnlyDivisionButton;
    Button AllOperatorsModeButton;



    private int _difficulty;

    public static List<string> _operators = _operators = new List<string>();

    public void Start()
    {      
        RandomSpawn.acceleration = true;
        _difficulty = 1;      
        RandomSpawn.difficulty = _difficulty;
        UpdateDifficultyText(_difficulty);
    }
    
    public void SetSummationOnlyMode()
    {
        _operators.Clear();
        _operators.Add("+");
        UnityEngine.Debug.Log("IN LC" + string.Join(",", _operators.ToArray()));
    }
    public void SetSubstactionOnlyMode()
    {
        _operators.Clear();
        _operators.Add("-");
        UnityEngine.Debug.Log("IN LC" + string.Join(",", _operators.ToArray()));
    }
    public void SetMultiplicationOnlyMode()
    {
        _operators.Clear();
        _operators.Add("*");
        UnityEngine.Debug.Log("IN LC" + string.Join(",", _operators.ToArray()));
    }
    public void SetDivisionOnlyMode()
    {
        _operators.Clear();
        _operators.Add("/");
        UnityEngine.Debug.Log("IN LC" + string.Join(",", _operators.ToArray()));
    }
    public void SetAllOperatorsMode()
    {
        _operators.Clear();
        _operators.Add("+");
        _operators.Add("-");
        _operators.Add("*");
        _operators.Add("/");
        UnityEngine.Debug.Log("IN LC" + string.Join(",", _operators.ToArray()));
    }

    public void OnToggleClick()
    {
        if (toggle.isOn == true)
        {
            RandomSpawn.acceleration = true;
        }
        else
        {
            RandomSpawn.acceleration = false;
        }
    }
    public void UpdateDifficultyText(int changedDifficulty)
    {     
        switch(changedDifficulty)
        {
            case 1:
            {
                difficulty_text.text = "Легкий"; 
                break;
            }
             case 2:
            {
                difficulty_text.text = "Средний"; 
                break;
            }
             case 3:
            {
                difficulty_text.text = "Сложный"; 
                break;
            }
            default: break;
        }
    }
    public void RightButton()
    {
        if(_difficulty != 3)
        {
            _difficulty = UnityEngine.Mathf.Min(3, _difficulty+1);
            RandomSpawn.difficulty = _difficulty;
            UpdateDifficultyText(_difficulty);
        }
        else
        {
            _difficulty = 1;
            RandomSpawn.difficulty = _difficulty;
            UpdateDifficultyText(_difficulty);
        }
    }

    public void LeftButton()
    {
        if(_difficulty != 1)
        {
            _difficulty = UnityEngine.Mathf.Max(1, _difficulty-1);
            RandomSpawn.difficulty = _difficulty;
            UpdateDifficultyText(_difficulty);
        }
        else
        {
            _difficulty = 3;
            RandomSpawn.difficulty = _difficulty;
            UpdateDifficultyText(_difficulty);
        }      
    }

    public void StartGame()
    {
        if(_operators.Count > 0)
        {
            ExpressionLogicClass.operators = _operators;
            SceneManager.LoadScene("GameScene");
        }
        
    }

    public void PortStartGame()
    {
        if (_operators.Count > 0)
        {
            ExpressionLogicClass.operators = _operators;
            SceneManager.LoadScene("PortGameScene");
        }
        
    }

    public void Back()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void PortBack()
    {
        SceneManager.LoadScene("MainMenuPort");
    }
}
