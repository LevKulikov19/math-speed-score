using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelsUI : MonoBehaviour
{

    public Button OkButton;
    public Button minusButton;
    public Button plusButton;
    public Button multiplyButton;
    public Button divideButton;
    public GameObject Panel;
    public GameObject ElementsToHide;

    private bool plusButtonStatus = false;
    private bool minusButtonStatus = false;
    private bool multButtonStatus = false;
    private bool divButtonStatus = false;
    private bool isPanelOpen = false;

    


    private void Start()
    {
        ResetButtonStatus();
        Panel.SetActive(false);
        minusButton.onClick.AddListener(OnMinusButtonClick);
        plusButton.onClick.AddListener(OnPlusButtonClick);
        multiplyButton.onClick.AddListener(OnMultiplyButtonClick);
        divideButton.onClick.AddListener(OnDivideButtonClick);
        
    }
    private void ResetButtonStatus()
    {
        plusButtonStatus = false;
        minusButtonStatus = false;
        multButtonStatus = false;
        divButtonStatus = false;
        ResetButtonText(new Button[4]{plusButton,minusButton,multiplyButton,divideButton});
    }
    private void ResetButtonText(Button[] buttons)
    {
        foreach (Button bt in buttons)
        {
            Text buttonText = bt.GetComponentInChildren<Text>();
            buttonText.text = "Добавить";
            buttonText.color = Color.green; 
        }
    }
    public void OpenPanel()
    {
        UnityEngine.Debug.Log("LC" + string.Join(",", LevelsChanged._operators.ToArray())); 
        ResetButtonStatus(); 
        CheckExistOperators();  
        Panel.SetActive(true);
        isPanelOpen = true;
        CloseOtherCanvasElements();
    }

    private void CheckExistOperators()
    {
        if(LevelsChanged._operators.Contains("+"))
        {
            OnPlusButtonClick();
        }
        if(LevelsChanged._operators.Contains("-"))
        {
            OnMinusButtonClick();
        }
        if(LevelsChanged._operators.Contains("*"))
        {
            OnMultiplyButtonClick();
        }
        if(LevelsChanged._operators.Contains("/"))
        {
            OnDivideButtonClick();
        }
    }

    public void CloseOtherCanvasElements()
    {
        ElementsToHide.SetActive(false);  
    }

    public void OpenOtherCanvasElements()
    {
        ElementsToHide.SetActive(true);  
    }

    public void ClosePanel()
    {
        Panel.SetActive(false);
        isPanelOpen = false;
    }

    public void OkButtonClick()
    {
        ClosePanel();
        OpenOtherCanvasElements();
    }
    

    private void OnPlusButtonClick()
    {
        plusButtonStatus = !plusButtonStatus;
        UpdateButtonState(plusButton, plusButtonStatus, "+");
    }

    private void OnMinusButtonClick()
    {
        minusButtonStatus = !minusButtonStatus;
        UpdateButtonState(minusButton, minusButtonStatus, "-");
    }

    private void OnMultiplyButtonClick()
    { 
        multButtonStatus = !multButtonStatus;
        UpdateButtonState(multiplyButton, multButtonStatus, "*");
    }

    private void OnDivideButtonClick()
    {             
        divButtonStatus = !divButtonStatus;
        UpdateButtonState(divideButton, divButtonStatus, "/");
    }

    private void UpdateButtonState(Button button, bool status, string act)
    {
        Text buttonText = button.GetComponentInChildren<Text>();
        if(status)
        {
            buttonText.text = "Удалить";      
            buttonText.color = Color.red;
            if(!LevelsChanged._operators.Contains(act))
            {
                LevelsChanged._operators.Add(act);
            }       
        }
        else
        {
            buttonText.text = "Добавить";      
            buttonText.color = Color.green;
            LevelsChanged._operators.Remove(act);
        }
    }
}